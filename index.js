function myArrayFilter(arr, callback)
{
	var i;
	var answer=[];
	for(i=0; i<arr.length; i++)
	{
		if(callback(arr[i],i,arr)==true)
		{
			answer.push(arr[i]);
		}
	}
	return answer;
}

function myArrayReduce(arr, callback, acc)
{
	var temp,i,temp2;
	if(arguments.length==3)
	{
		for(i=0; i<arr.length; i++)
		{
			temp=callback(acc,arr[i],i,arr);
			acc=temp;
		}
		return acc;
	}
	else
	{
		var ac=arr[0];
		for(i=1; i<arr.length; i++)
		{
			temp=callback(ac,arr[i],i,arr);
			ac=temp;
		}
		return ac;
	}


}

function myTreeReduce(inFunc, endFunc)
{
	function something(tree)
	{
		var x,y;
		//		x=something(tree.left);
		if(tree.left!=undefined)
		{
			x=something(tree.left);
		}
		if(tree.right!=undefined)
		{
			y=something(tree.right);
		}
		if(tree.type=='in')
		{
			if(x!=undefined && y!=undefined)
			{
				return inFunc(tree.value,x,y);
			}
			if(x!=undefined && y==undefined)
			{
				return inFunc(tree.value,x);
			}
			if(x==undefined && y!=undefined)
			{
				return inFunc(tree.value,y);
			}
		}
		if(tree.type=='end')
		{
			return endFunc(tree.value);
		}
	}
	return something;
}



function myTreeSize(tree)
{
/*	var x=0;
	if(tree.left!=undefined)
	{
		x=x+myTreeSize(tree.left);
	}
	if(tree.right!=undefined)
	{
		x=x+myTreeSize(tree.right);
	}
	return x+1;*/
	const add = (a, b, c) => 1 + b + c;
	var x=myTreeReduce(add,x=>1);
	return x(tree);

}

function myTreeTraversal(type)
{
	function preorder(tree)
	{
		let arr=[];
		if(tree!=undefined)
		{
			arr.push(tree.value);
			arr=arr.concat(preorder(tree.left));
			arr=arr.concat(preorder(tree.right));
		}
		return arr;
	}
	if(type=='pre')
	{
		return preorder;
	}
	function postorder(tree)
	{
		let arr=[];
		if(tree!=undefined)
		{
			arr=arr.concat(postorder(tree.left));
			arr=arr.concat(postorder(tree.right));
			arr.push(tree.value);
		}
		return arr;
	}
	if(type=='post')
	{
		return postorder;
	}
	function inorder(tree)
	{
		let arr=[];
		if(tree!=undefined)
		{
			arr=arr.concat(inorder(tree.left));
			arr.push(tree.value);
			arr=arr.concat(inorder(tree.right));
		}
		return arr;
	}
	if(type=='in')
	{
		return inorder;
	}

}

function hangman(phrase) {

	const gameOver = "Game over!!!";
	const won = "You\'ve got it!!! Final phrase:";
	const wrong = "Incorrect guess!!!";
	const lost = "You\'ve lost!!! Correct phrase:";
	// ig = incorrect guesses
	var ig=0;
	var guesses=[];
	var wrongnum=0,over=0;
	function guess(c)
	{
		if(over==1)
		{
			return gameOver;
		}

		let flag=0,i;
		for(i=0; i<phrase.length; i++)
		{
			if(phrase[i]==c)
			{
				flag=1;
			}
		}
		if(flag==0)
		{
			let flag2=0;
			for(i=0; i<guesses.length; i++)
			{
				if(guesses[i]==c)
				{
					flag2=1;
				}
			}
			if(flag2==1)
			{
				return wrong;
			}
			else
			{
				guesses.push(c);
				wrongnum++;
				if(wrongnum==3)
				{
					over=1;
					return lost+" "+phrase;
				}
				else
				{
					return wrong;
				}
			}
		}
		else
		{
			guesses.push(c);
			let i,j,f=0,answer=[];
			for(i=0; i<phrase.length; i++)
			{
				f=0;
				for(j=0; j<guesses.length; j++)
				{
					if(guesses[j]==phrase[i])
					{
						f=1;
					}
				}
				
				if(f==1)
				{
					answer.push(phrase[i]);
				}
				else
				{
					answer.push('_');
				}
			}
			f=0;
			for(i=0; i<answer.length; i++)
			{	
				if(answer[i]=='_')
				{
					f=1;
				}
			}
			if(f==1)
			{
				return answer.join(' ');
			}
			else
			{
				over=1;
				return won+" "+phrase;
			}
		}


	}
	return guess;
}

function Person(name, age)
{
	this.name=name;
	this.age=age;
}
Person.prototype.about=function(){
	return 'My name is '+ this.name+' and I\'m '+this.age+' yrs old.';
}


function Student(name, age, roll)
{
	Person.call(this,name,age);
	this.roll=roll;
}
Student.prototype.about=function(){ return Person.prototype.about.call(this);}
Student.prototype.id=function(){return 'Student Id: '+this.roll;}

const numberList = {
	numbers:[],
	set add(num)
	{
		this.numbers.push(num);
	},
	get sum()
	{
		let x=0;
		for(i=0; i<this.numbers.length; i++)
		{
			x=x+this.numbers[i];
		}
		return x;
	},
	get average()
	{
		let x=0;
		return this.sum/this.numbers.length;
	}


};

function carRace(cars, finish)
{
	var i,car,t,s,fastest;
	let race=Promise.race(cars);
	race.then(function(obj){finish(obj.name+' won in '+obj.time+' seconds!!!');});
//	console.log(t);
//	finish(fastest.name+' won in '+fastest.time+' seconds!!!');

}

module.exports = {
	myArrayFilter,
	myArrayReduce,
	myTreeReduce,
	myTreeSize,
	myTreeTraversal,
	hangman,
	Person,
	Student,
	numberList,
	carRace
};
